import {Injectable} from '@angular/core';
import {BehaviorSubject} from 'rxjs';
import {DataInterface} from './interfaces/data.interface';

@Injectable()
export class AppService {
  data = new BehaviorSubject<DataInterface>(null);

  set(data) {
    this.data.next(data);
  }

  get() {
    return this.data.asObservable();
  }
}
