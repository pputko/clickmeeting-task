import {Component} from '@angular/core';
import {AppService} from '../../app.service';
import {Router} from '@angular/router';
import {MatSnackBar} from '@angular/material';

@Component({
  selector: 'app-setup',
  templateUrl: './setup.component.html',
  styleUrls: ['./setup.component.scss']
})
export class SetupComponent {
  public shapes: string[] = ['circle', 'square', 'rectangle'];
  public shape =  '';
  public resultType = '';
  public a: number;
  public b: number;

  constructor(private appService: AppService, private router: Router, private snackBar: MatSnackBar) {}

  getResult() {
    if (this.a <= 0 || this.b <= 0) {
      this.snackBar.open('You cannot provide negative value!', 'Error', {
        duration: 2000,
      });
      return;
    }
    switch (this.shape) {
      case 'circle':
        this.appService.set({
          shape: {
            a: this.a,
            name: this.shape
          },
          resultType: this.resultType
        });
        break;
      case 'square':
        this.appService.set({
          shape: {
            a: this.a,
            name: this.shape
          },
          resultType: this.resultType
        });
        break;
      case 'rectangle':
        this.appService.set({
          shape: {
            a: this.a,
            b: this.b,
            name: this.shape
          },
          resultType: this.resultType
        });
        break;
    }
    this.router.navigate(['/result']);
  }
}
