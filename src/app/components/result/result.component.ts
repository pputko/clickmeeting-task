import {Component, OnDestroy, OnInit} from '@angular/core';
import {AppService} from '../../app.service';
import {DataInterface} from '../../interfaces/data.interface';
import {Subscription} from 'rxjs';
import {Circle} from '../../classes/circle';
import {Rectangle} from '../../classes/rectangle';
import {Square} from '../../classes/square';
import {Router} from '@angular/router';

@Component({
  selector: 'app-result',
  templateUrl: './result.component.html',
  styleUrls: ['./result.component.scss']
})
export class ResultComponent implements OnInit, OnDestroy {
  public data: DataInterface;
  public result: number;
  private subscription: Subscription;
  constructor(private appService: AppService, private router: Router) {}

  ngOnInit() {
    this.subscription = this.appService.get().subscribe(res => {
      if (!res) {
        this.goBack();
        return;
      }
      this.data = res;
      this.data.resultType === 'area' ? this.getAreaResult() : this.getCircuitResult();

    });
  }

  ngOnDestroy(): void {
    this.subscription.unsubscribe();
  }

  getAreaResult() {
    switch (this.data.shape.name) {
      case 'circle':
        const circle = new Circle(this.data.shape.name, this.data.shape.a);
        this.result = circle.getArea();
        break;
      case 'rectangle':
        const rect = new Rectangle(this.data.shape.name, this.data.shape.a, this.data.shape.b);
        this.result = rect.getArea();
        break;
      case 'square':
        const square = new Square(this.data.shape.name, this.data.shape.a);
        this.result = square.getArea();
        break;
    }
  }

  getCircuitResult() {
    switch (this.data.shape.name) {
      case 'circle':
        const circle = new Circle(this.data.shape.name, this.data.shape.a);
        this.result = circle.getCircuit();
        break;
      case 'rectangle':
        const rect = new Rectangle(this.data.shape.name, this.data.shape.a, this.data.shape.b);
        this.result = rect.getCircuit();
        break;
      case 'square':
        const square = new Square(this.data.shape.name, this.data.shape.a);
        this.result = square.getCircuit();
        break;
    }
  }

  goBack() {
    this.router.navigate(['/setup']);
  }
}
