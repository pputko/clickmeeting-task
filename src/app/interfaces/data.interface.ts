import {ShapeInterface} from './shape.interface';

export interface DataInterface {
  shape: ShapeInterface;
  resultType: string;
}
