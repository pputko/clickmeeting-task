import {Shape} from './shape';

export class Rectangle extends Shape {
  constructor(name, a, b) {
    super(name, a, b);
  }
}
