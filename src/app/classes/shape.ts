export class Shape {
  public name: string;
  public a: number;
  public b: number;

  constructor(name, a, b?) {
    this.name = name;
    this.a = a;
    this.b = b;
  }

  getArea() {
    switch (this.name) {
      case 'circle':
        return Number((Math.PI * Math.pow(this.a, 2)).toFixed(2));
      case 'square':
        return Math.pow(this.a, 2);
      case 'rectangle':
        return this.a * this.a;
    }
  }

  getCircuit() {
    switch (this.name) {
      case 'circle':
        return Number((2 * Math.PI * this.a).toFixed(2));
      case 'square':
        return 4 * this.a;
      case 'rectangle':
        return (2 * this.a) + (2 * this.b);
    }
  }
}
