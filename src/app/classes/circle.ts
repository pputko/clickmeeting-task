import {Shape} from './shape';

export class Circle extends Shape {
  constructor(name, r) {
    super(name, r);
  }
}
