import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import {SetupComponent} from './components/setup/setup.component';
import {ResultComponent} from './components/result/result.component';

const routes: Routes = [
  {path: '', redirectTo: 'setup', pathMatch: 'full'},
  {path: 'setup', component: SetupComponent},
  {path: 'result', component: ResultComponent}
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
