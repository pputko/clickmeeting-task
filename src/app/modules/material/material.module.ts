import {NgModule} from '@angular/core';
import {
  MatButtonModule,
  MatInputModule,
  MatRadioModule,
  MatSelectModule,
  MatFormFieldModule,
  MatIconModule,
  MatSnackBarModule
} from '@angular/material';

@NgModule({
  imports: [
    MatSelectModule,
    MatInputModule,
    MatButtonModule,
    MatRadioModule,
    MatFormFieldModule,
    MatIconModule,
    MatSnackBarModule
  ],
  exports: [
    MatSelectModule,
    MatInputModule,
    MatButtonModule,
    MatRadioModule,
    MatFormFieldModule,
    MatIconModule,
    MatSnackBarModule
  ]
})
export class MaterialModule {}
